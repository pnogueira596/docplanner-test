import axios from 'axios';

export function getDoctors(searchText: string) {
    return axios.post('https://docplanner-3.algolia.io/1/indexes/pl_autocomplete_doctor/query', {
        apiKey: "90a529da12d7e81ae6c1fae029ed6c8f",
        appID: "docplanner",
        params: "query=" + searchText + "&hitsPerPage=4"
    });
}

export function getFacilities(searchText: string) {
    return axios.post('https://docplanner-3.algolia.io/1/indexes/pl_autocomplete_facility/query', {
        apiKey: "90a529da12d7e81ae6c1fae029ed6c8f",
        appID: "docplanner",
        params: "query=" + searchText + "&hitsPerPage=4"
    });
}