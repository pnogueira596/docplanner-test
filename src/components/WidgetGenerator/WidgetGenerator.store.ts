import { Action, Module, Mutation, VuexModule } from "vuex-class-modules";
import store from '@/store/index'
import { Doctor } from "@/models/Doctor";
import { WidgetTypeEnum } from "@/models/Widget";
import { Facility } from "@/models/Facility";

@Module
class WidgetGeneratorStore extends VuexModule {
    BASE_URL: string = '//widgets.znanylekarz.pl/';

    selectedProfile: Doctor | Facility | null  = null;
    selectedType: WidgetTypeEnum | null = WidgetTypeEnum.WITH_CALENDAR;
    widgetUrl: string = '';
    shouldShowWidgetCode: boolean = false;
    widgetEmbededCode: string = '';

    @Mutation
    setSelectedProfile(profile: (Doctor | Facility) | null) {
        this.selectedProfile = profile;
    }

    @Mutation
    setSelectedType(type: WidgetTypeEnum) {
        this.selectedType = type;
    }

    @Mutation
    setWidgetEmbededCode() {
        if (this.selectedProfile instanceof Doctor) {
            this.widgetEmbededCode = '<a id="zl-url" class="zl-url" href="'+ this.selectedProfile?.url +'" rel="nofollow" data-zlw-doctor="'+ this.selectedProfile?.urlName +'" data-zlw-type="'+ this.selectedType +'" data-zlw-opinion="false" data-zlw-hide-branding="true">'+ this.selectedProfile?.name + ' ' + this.selectedProfile?.surname +' - ZnanyLekarz.pl</a><script>!function($_x,_s,id){var js,fjs=$_x.getElementsByTagName(_s)[0];if(!$_x.getElementById(id)){js = $_x.createElement(_s);js.id = id;js.src = "//platform.docplanner.com/js/widget.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","zl-widget-s");</script>'; 
        } else {
            this.widgetEmbededCode = '<a class="zl-facility-url" href="'+ this.selectedProfile?.url +'" rel="nofollow" data-zlw-facility="'+ this.selectedProfile?.urlName +'" data-zlw-type="'+ this.selectedType +'" data-zlw-saas-only="false">'+ this.selectedProfile?.name +'</a><script>!function($_x,_s,id){var js,fjs=$_x.getElementsByTagName(_s)[0];if(!$_x.getElementById(id)){js = $_x.createElement(_s);js.id = id;js.src = "//platform.docplanner.com/js/widget.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","zl-widget-s");</script>'
        }
    }

    @Mutation
    getWidget() {
        if (this.selectedProfile instanceof Doctor) {
            switch(this.selectedType) {
                case WidgetTypeEnum.WITH_CALENDAR:
                    this.widgetUrl = '//www.znanylekarz.pl/ajax/marketing/doctor/widget/big_with_calendar/' + this.selectedProfile?.urlName + '?hide_branding=true';
                    break;
                default:
                    this.widgetUrl = this.BASE_URL + 'doctor/widget/' + this.selectedType + '/' + this.selectedProfile?.urlName
                    break;
            }
        } else {
            let type: string = '';

            if(this.selectedType === WidgetTypeEnum.WITH_CALENDAR) {
                type = 'facility-big';
            } else {
                type = this.selectedType!;
            }

            switch(this.selectedType) {
                case WidgetTypeEnum.WITH_CALENDAR:
                    type = 'facility-big';
                    break;
                case WidgetTypeEnum.SIMPLE_LINK:
                    type = 'facility_simple_link';
                    break;
                case WidgetTypeEnum.BUTTON:
                    type = 'facility-button';
                    break;
            }

            this.widgetUrl = this.BASE_URL + 'facility/widget/' + type + '/' + this.selectedProfile?.urlName + '?referrer=' + this.selectedProfile?.url
        }
    }

    @Mutation
    setShouldShowWidgetCode(flag: boolean) {
        this.shouldShowWidgetCode = flag;
    }
}

export const widgetGeneratorStore = new WidgetGeneratorStore({ store, name: "widgetGeneratorStore" });