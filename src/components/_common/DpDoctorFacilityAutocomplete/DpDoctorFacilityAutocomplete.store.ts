import { Action, Module, Mutation, VuexModule } from "vuex-class-modules";
import store from '@/store/index'

import { Doctor } from "@/models/Doctor";
import { Facility } from "@/models/Facility";

import { getDoctors, getFacilities } from "@/api/Algolia.api";

@Module
class DpDoctorFacilityAutocompleteStore extends VuexModule {
    searchText: string = '';
    doctorFacilityList: (Doctor | Facility)[] = [];

    @Mutation
    setSearchText(searchText: string) {
        this.searchText = searchText;
    }

    @Mutation
    setDoctoFacilityList(list: (Doctor | Facility)[]) {
        this.doctorFacilityList = list;
    }

    @Action
    async searchDoctorFacilities(searchString: string) {
        this.setSearchText(searchString);

        const resultDotors = await getDoctors(searchString);
        const resultFacilities = await getFacilities(searchString);
        let doctorsFacilities: (Doctor | Facility)[] = [];

        resultDotors.data.hits.map((item: any) => {
            doctorsFacilities.push(new Doctor({
                id: item.objectID,
                name: item.name,
                surname: item.name,
                url: item.url,
                image: item.image_micro_square,
                urlName: item.urlname
            }))
        })

        resultFacilities.data.hits.map((item: any) => {
            doctorsFacilities.push(new Facility({
                id: item.objectID,
                name: item.name,
                url: item.url,
                image: item.image_micro_square,
                urlName: item.urlname
            }))
        })

        if (searchString === '') {
            doctorsFacilities = [];
        }

        this.setDoctoFacilityList(doctorsFacilities);
    }
}

export const dpDoctorFacilityAutocompleteStore = new DpDoctorFacilityAutocompleteStore({ store, name: "dpDoctorFacilityAutocompleteStore" });