export enum WidgetTypeEnum {
    WITH_CALENDAR = 'big_with_calendar',
    SIMPLE_LINK = 'simple_link',
    BUTTON = 'button_calendar_medium'
}