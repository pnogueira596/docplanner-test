export interface IFacility {
    id: string;
    name: string,
    url: string,
    image: string,
    urlName: string
}

export class Facility implements IFacility {
    public id: string;
    public name: string;
    public url: string;
    public image: string;
    public urlName: string;

    constructor(facility: IFacility) {
        this.id = facility.id;
        this.name = facility.name;
        this.url = facility.url;
        this.image = facility.image;
        this.urlName = facility.urlName;
    }
}