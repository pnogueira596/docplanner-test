export interface IDoctor {
    id: string;
    name: string,
    surname: string,
    url: string,
    image: string,
    urlName: string
}

export class Doctor implements IDoctor {
    public id: string;
    public name: string;
    public surname: string;
    public url: string;
    public image: string;
    public urlName: string;

    constructor(doctor: IDoctor) {
        this.id = doctor.id;
        this.name = doctor.name;
        this.surname = doctor.surname;
        this.url = doctor.url;
        this.image = doctor.image;
        this.urlName = doctor.urlName;
    }
}