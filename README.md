# Docplanner phone documentation

## Tech used

- Vue (with Vue CLI)
- Typescript
- SCSS
- Axios
- Bootstrap

## Assumptions

- I assumed that can get the list doctors and facilities directly from Algolia.
- I assumed that you have a static list of widget types because I couldn't catch it on the requests
- I assumed that you are building the widget link on the front-end because I also couldn't find it in the requests

## Possible impovements

- Having unit tests for at least the common components
- Having a service to only build the widget links
- Having the widget types from the Api depending on what type of profile is selected
- Having a transformer/adapter for the Algolia Api so we don't need to mess arround with the response structure.

## How to run the project

- Inside of the project folder run `npm install`
- After the install is completed, run `npm run serve`
- In the browser go to `http://localhost:8080`